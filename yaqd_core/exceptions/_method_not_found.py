__all__ = ["MethodNotFound"]


class MethodNotFound(BaseException):
    pass
